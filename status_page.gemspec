$LOAD_PATH.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'status_page/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'status_page'
  s.version     = StatusPage::VERSION
  s.authors     = ['Krzysztof Baranski']
  s.email       = ['baranski.kk@gmail.com']
  s.homepage    = 'https://bitbucket.org/baranski/status_page'
  s.summary     = 'Summary of StatusPage.'
  s.description = 'Description of StatusPage.'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'rails', '~> 4.2.7.1'

  s.add_development_dependency 'sqlite3'
end
